﻿using System.Collections.Generic;
using System.Linq;

namespace Saxo.Helpers
{
    public static class Common
    {
        public static IEnumerable<IEnumerable<T>> SplitCollection<T>(this IEnumerable<T> source, int size)
        {
            return source.Select((x, i) => new { Index = i, Value = x })
                         .GroupBy(x => x.Index / size)
                         .Select(x => x.Select(v => v.Value).ToList());
        }
    }
}