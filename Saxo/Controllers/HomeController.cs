﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Saxo.Service;
using Saxo.Service.Abstract;

namespace Saxo.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBookService _service;
        public HomeController()
        {
            _service = new BookService();
        }
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> GetBooks(List<string> isbns)
        {
            if (isbns == null || !isbns.Any() || isbns.FirstOrDefault() == string.Empty)
            {
                return new EmptyResult();
            }
            return PartialView("_Books", await _service.GetBooksAsync(isbns.Distinct().ToList()));
        }
    }
}