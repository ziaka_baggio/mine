﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saxo.Models;

namespace Saxo.Service.Abstract
{
    public interface IBookService
    {
        Task<List<BookViewModel>> GetBooksAsync(List<string> isbns);
    }
}
