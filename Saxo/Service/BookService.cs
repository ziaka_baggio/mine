﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Saxo.Database.Repositories;
using Saxo.Database.Repositories.Abstract;
using Saxo.Helpers;
using Saxo.Models;
using Saxo.Service.Abstract;

namespace Saxo.Service
{
    public class BookService: IBookService
    {
        private const string Uri = "http://api.saxo.com/v1/products/products.json";
        private const string Key = "08964e27966e4ca99eb0029ac4c4c217";
        private readonly IBookRepository _repository;

        public BookService()
        {
            _repository = new BookRepository();
        }

        public async Task<List<BookViewModel>> GetBooksAsync(List<string> isbns)
        {
            var bookslist = new List<BookViewModel>();
            var booksFromDb = _repository.GetBooks(isbns);
            if (booksFromDb.Any())
            {
                bookslist.AddRange(booksFromDb.Select(item => new BookViewModel(item)));
                var foundIsbnsInDb = booksFromDb.Select(x => x.Isbn13).ToList();
                     isbns = isbns.Except(foundIsbnsInDb).ToList();
            }
            if (!isbns.Any())
            {
                return bookslist;
            }
            using (var httpClient = new HttpClient())
            {
                var isbnsCollection = isbns.SplitCollection(70);
                foreach (var collectionItem in isbnsCollection)
                {
                    var parameters = string.Join(",", collectionItem);
                    var requestedUri = string.Format("{0}?key={1}&isbn={2}", Uri, Key, parameters);
                    var data = JsonConvert.DeserializeObject<ServiceData>(await httpClient.GetStringAsync(requestedUri));
                    if (data.Products.Any())
                    {
                        var bookEntities = data.Products.Select(item => item.ToEntity()).ToList();
                        _repository.AddBooks(bookEntities);
                        bookslist.AddRange(data.Products);
                        _repository.Commit();
                    }
                }
            }
            return bookslist;
        }


    }
}