﻿using System;
using System.Data.Entity;
using Saxo.Database.Repositories.Abstract;

namespace Saxo.Database.Repositories
{
    public abstract class ContextFactory<TContext> : IDisposable where TContext : DbContext, IDisposedTracker, new()
    {
        private TContext _dataContext;

        protected virtual TContext DataContext
        {
            get
            {
                if (_dataContext == null || _dataContext.IsDisposed)
                {
                    _dataContext = new TContext();
                }
                return _dataContext;
            }
        }
        
        public virtual void Dispose()
        {
            if (DataContext != null) 
                DataContext.Dispose();
        }

    }
}
