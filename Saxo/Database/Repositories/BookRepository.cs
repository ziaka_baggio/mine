﻿using System.Collections.Generic;
using System.Linq;
using Saxo.Database.Entity;
using Saxo.Database.Repositories.Abstract;
using Saxo.Models;

namespace Saxo.Database.Repositories
{
    public class BookRepository : ContextFactory<AppContext>, IBookRepository
    {

        public void AddBooks(List<Book> books)
        {
            DataContext.Books.AddRange(books);
        }

        public List<Book> GetBooks(List<string> isbns)
        {
            var booksList = DataContext.Books.Where(x => isbns.Contains(x.Isbn13)).ToList();
            return booksList;
        }

        public void Commit()
        {
            DataContext.SaveChanges();
        }


    }
}