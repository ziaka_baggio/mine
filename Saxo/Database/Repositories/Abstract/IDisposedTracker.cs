﻿namespace Saxo.Database.Repositories.Abstract
{
    public interface IDisposedTracker
    {
        bool IsDisposed { get; set; }
    }
}
