﻿using System.Collections.Generic;
using Saxo.Database.Entity;

namespace Saxo.Database.Repositories.Abstract
{
    public interface IBookRepository
    {
        void AddBooks(List<Book> books);
        List<Book> GetBooks(List<string> books);
        void Commit();
    }
}
