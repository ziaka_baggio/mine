﻿using System;

namespace Saxo.Database.Entity
{
    public class Book
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Isbn13 { get; set; }

        public DateTime AddedDateTime
        {
            get { return DateTime.Now; }
        }
    }
}