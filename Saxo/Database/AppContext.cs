﻿using System.Data.Entity;
using Saxo.Database.Entity;
using Saxo.Database.Repositories.Abstract;

namespace Saxo.Database
{
    public class AppContext : DbContext, IDisposedTracker
    {
        public AppContext():  base("DefaultConnection")
        {
        }
        public DbSet<Book> Books { get; set; }
        public bool IsDisposed { get; set; }
    }
}