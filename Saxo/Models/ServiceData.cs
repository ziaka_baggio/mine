﻿using System.Collections.Generic;

namespace Saxo.Models
{
    public class ServiceData
    {
        public List<BookViewModel> Products { get; set; }
    }
}