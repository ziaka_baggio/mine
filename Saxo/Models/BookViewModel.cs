﻿using Saxo.Database.Entity;

namespace Saxo.Models
{
    public class BookViewModel
    {
        public BookViewModel()
        {
            
        }
        public BookViewModel(Book entity)
        {
            Id = entity.Id;
            ImageUrl = entity.ImageUrl;
            Title = entity.Title;
            Isbn13 = entity.Isbn13;
        }
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Isbn13 { get; set; }

        public Book ToEntity()
        {
            return new Book
            {
                Id = Id,
                ImageUrl = ImageUrl,
                Title = Title,
                Isbn13 = Isbn13
            };
        }
    }
}